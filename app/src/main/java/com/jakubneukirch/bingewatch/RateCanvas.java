package com.jakubneukirch.bingewatch;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;

/**
 * Created by 19ort on 03.08.2016.
 */
public class RateCanvas extends View {
    float left;
    float top;
    float bottom;
    float right;
    float strokeWidth;
    float angle;
    float density;

    public RateCanvas(Context context,float x, float y, float height, float width,float strokeWidth, float angle ) {
        super(context);
        this.density = context.getResources().getDisplayMetrics().density;
        this.left = x * density;
        this.top = y *density;
        this.bottom = top + (height*density);
        this.right = left + (width*density);
        this.strokeWidth = strokeWidth*density;
        this.angle = angle;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        RectF rect = new RectF();
        rect.set(left,top,right,bottom);

        Paint yellow = new Paint();
        yellow.setDither(true);
        yellow.setColor(Color.parseColor("#FF8778"));
        yellow.setStyle(Paint.Style.STROKE);
        //yellow.setStrokeCap(Paint.Cap.ROUND);
        //yellow.setStrokeJoin(Paint.Join.ROUND);
        yellow.setStrokeWidth(strokeWidth);
        yellow.setAntiAlias(true);

        Paint red = new Paint();
        red.setStyle(Paint.Style.FILL);
        red.setColor(Color.parseColor("#E86272"));


        canvas.drawOval(rect,red);
        canvas.drawArc(rect,270,angle,false,yellow);

    }
}
