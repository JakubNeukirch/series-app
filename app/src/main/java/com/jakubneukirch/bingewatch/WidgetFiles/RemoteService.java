package com.jakubneukirch.bingewatch.WidgetFiles;

import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViewsService;

import com.jakubneukirch.bingewatch.JSONconverter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by 19ort on 22.05.2016.
 */
public class RemoteService extends RemoteViewsService {



    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        Log.d("RemoteService", "onGetViewFactory()");
        ArrayList<HashMap<String,Object>> list = new ArrayList<>();
        JSONconverter jc = new JSONconverter();
        ArrayList<ArrayList<HashMap<String,Object>>> jlist=null;
        try{
            jlist = jc.getFileSeries(this.getApplicationContext());
        }catch(Exception ex){
            Log.d("RemoteService", ex.toString());
        }
        if(jlist!=null){
            for(int i =0;i<jlist.size();i++){
                if(jlist.get(i).get(0)!=null)
                    list.add(jlist.get(i).get(0));

            }
        }

        int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,AppWidgetManager.INVALID_APPWIDGET_ID);
        return (new RemoteFactory(this.getApplicationContext(),intent,list));
    }
}
