package com.jakubneukirch.bingewatch.WidgetFiles;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.RemoteViews;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

import serieminder.com.seriale.R;

import com.jakubneukirch.bingewatch.JSONconverter;
import com.jakubneukirch.bingewatch.WidgetFiles.RemoteService;

/**
 * Created by 19ort on 22.05.2016.
 */
public class WidgetService extends AppWidgetProvider {

    public static final String MY_PUBLIC_STATIC_STRING = "UPDATETHIS";

    int s = 0;
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        int N = appWidgetIds.length;
        for(int i=0;i<N;i++){
            int appWidgetId = appWidgetIds[i];

            RemoteViews views = updateListView(context,appWidgetId);

            ArrayList<HashMap<String,Object>> episodes = updateEpisodes(context);



            appWidgetManager.updateAppWidget(appWidgetId,views);
            Log.d("WidgetService","onUpdate");
            super.onUpdate(context,appWidgetManager,appWidgetIds);
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds,R.id.widgetListView);
        }

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        Log.d("WidgetService","onReceive()");
            update(context);



    }

    private void update(Context context){
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, this.getClass()));
        int N = appWidgetIds.length;
        for(int i=0;i<N;i++){
            int appWidgetId = appWidgetIds[i];

            RemoteViews views = updateListView(context,appWidgetId);

            ArrayList<HashMap<String,Object>> episodes = updateEpisodes(context);
            Log.d("widget","update");
            appWidgetManager.updateAppWidget(appWidgetId,views);
            //super.onUpdate(context,appWidgetManager,appWidgetIds);
        }
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds,R.id.widgetListView);
    }


    private RemoteViews updateListView(Context context, int appWidgetId){
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.activity_widget);

        Intent intent = new Intent(context,RemoteService.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,appWidgetId);
        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
       // views.setRemoteAdapter(appWidgetId,R.id.widgetListView,intent);
        views.setRemoteAdapter(R.id.widgetListView,intent);

        views.setEmptyView(R.id.widgetListView,R.id.empty_view);
        Log.d("widgetService","updateListView()");

        return views;
    }

    private ArrayList<HashMap<String,Object>> updateEpisodes(Context context){
        JSONconverter jc = new JSONconverter();
        ArrayList<HashMap<String,Object>> main_list= new ArrayList<>();
        ArrayList<ArrayList<HashMap<String,Object>>> list=new ArrayList<>();
        try{
           list = jc.getFileSeries(context);
        }catch(FileNotFoundException ex){
        }
        if(list!=null){
            for(int i =0;i<list.size();i++){
                for(int y = 0;y<list.get(i).size();y++){
                    if(list.get(i).get(0)!=null){
                        main_list.add(list.get(i).get(y));
                        Log.d("list",list.get(i).get(y).get("name")+"");
                    }

                }
            }
        }

        return main_list;
    }



}
