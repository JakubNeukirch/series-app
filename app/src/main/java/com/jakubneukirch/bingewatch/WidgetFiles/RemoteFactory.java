package com.jakubneukirch.bingewatch.WidgetFiles;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.jakubneukirch.bingewatch.JSONconverter;

import java.util.ArrayList;
import java.util.HashMap;

import serieminder.com.seriale.R;

/**
 * Created by 19ort on 22.05.2016.
 */
public class RemoteFactory implements RemoteViewsService.RemoteViewsFactory {

    private ArrayList<HashMap<String,Object>> itemList = new ArrayList<>();
    private Context context = null;
    private int appWidgetId;

    public RemoteFactory(Context context, Intent intent,ArrayList<HashMap<String,Object>> list) {
        this.context = context;
        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,AppWidgetManager.INVALID_APPWIDGET_ID);
        itemList = list;
        Log.d("RemoteFactory", "RemoteFactory()");
    }

    @Override
    public void onCreate() {

    }

    public void updateList(){

        itemList = new ArrayList<>();
        JSONconverter jc = new JSONconverter();
        ArrayList<ArrayList<HashMap<String,Object>>> jlist=null;
        try{
            jlist = jc.getFileSeries(context);
        }catch(Exception ex){
            Log.d("RemoteService", ex.toString());
        }
        if(jlist!=null){
            for(int i =0;i<jlist.size();i++){
                if(jlist.get(i).get(0)!=null)
                    itemList.add(jlist.get(i).get(0));

            }
        }
    }

    @Override
    public void onDataSetChanged() {
        Log.d("RemoteFactory", "onDataSetChanged()");
        updateList();

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {
       final RemoteViews remoteView = new RemoteViews(context.getPackageName(), R.layout.widget_list_item);
        String serie = (String)itemList.get(position).get("serie")+"";
        String name = (String)itemList.get(position).get("name")+"";
        String air_date = (String)itemList.get(position).get("air_date")+"";
        remoteView.setTextViewText(R.id.name,serie+" - "+name);
        remoteView.setTextViewText(R.id.date,air_date);
        Log.d("RemoteFactory", "getViewAt()");

        return remoteView;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
