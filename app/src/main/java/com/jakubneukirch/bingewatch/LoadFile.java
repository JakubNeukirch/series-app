package com.jakubneukirch.bingewatch;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by 19ort on 22.07.2016.
 */
public class LoadFile {

    private Context context;

    public LoadFile(Context context){
        this.context = context;
    }

    public ArrayList<HashMap<String,Object>> returnTodayEpisodes(){
        Log.d("ssss",loadFile().length()+"");
        ArrayList<HashMap<String,Object>> todayEps = new ArrayList<>();
        JSONconverter js = new JSONconverter();

        ArrayList<ArrayList<HashMap<String,Object>>> list = js.parseEpisodes(loadFile());
        Log.d("test",list.size()+" - ");
        Calendar calToday = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar epCal = Calendar.getInstance();

        for(int i=0;i<list.size();i++){
            for(int y=0;y<list.get(i).size();y++){
                try{
                    epCal.setTime(df.parse(list.get(i).get(y).get("air_date")+""));
                    if(epCal.get(Calendar.YEAR)==calToday.get(Calendar.YEAR)&&epCal.get(Calendar.DAY_OF_YEAR)==calToday.get(Calendar.DAY_OF_YEAR)){
                        todayEps.add(list.get(i).get(y));
                    }
                }catch (ParseException pe){

                }

                Log.d("test",list.get(i).get(y).get("air_date")+" - ");

            }
        }

        return todayEps;
    }

    private String loadFile(){
        StringBuilder sb= new StringBuilder();
        try{
            FileInputStream fis =context.openFileInput("series.json");
            InputStream is = fis;
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while((line=br.readLine())!=null){
                sb.append(line);
            }
            fis.close();
        }catch(Exception e){

        }
        return sb.toString();
    }
}
