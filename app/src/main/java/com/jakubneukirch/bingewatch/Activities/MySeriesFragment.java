package com.jakubneukirch.bingewatch.Activities;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jakubneukirch.bingewatch.JSONconverter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import serieminder.com.seriale.R;

/**
 * Created by Kuba on 2016-04-07.
 */
public class MySeriesFragment extends Fragment {
    private Toast test;

    private ExpandableListView exp_list_view;
    private TextView tx3;
    private ProgressBar mProgressBar;

    private ArrayList<HashMap<String,Integer>> lista;

    private boolean isReloading =false;

    JSONconverter jc = new JSONconverter();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        loadFile();
        getActivity().invalidateOptionsMenu();
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.my_series_fragment,container,false);
        tx3 = (TextView)v.findViewById(R.id.textView3);

        test = Toast.makeText(getActivity(),"some error", Toast.LENGTH_LONG);

        exp_list_view = (ExpandableListView)v.findViewById(R.id.expandableListView2);
        mProgressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        JSONconverter jc = new JSONconverter();

        reloadOffline();
        return v;
    }


    @Override
    public void onResume() {
        reloadOffline();
        getActivity().invalidateOptionsMenu();
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.more_my_series).setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        menu.findItem(R.id.more_my_series).setVisible(true);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0,v.getId(),0,"Delete");
        menu.add(0,v.getId(),0,"Go to serie...");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getTitle()=="Delete"){
            ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo)item.getMenuInfo();
            int groupPos = ExpandableListView.getPackedPositionGroup(info.packedPosition);
            deleteSerie(groupPos);
        }
        else if(item.getTitle()=="Go to serie..."){
            ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo)item.getMenuInfo();
            int groupPos = ExpandableListView.getPackedPositionGroup(info.packedPosition);
            Intent intent = new Intent(getActivity(),SerieActivity.class);
            intent.putExtra("id",lista.get(groupPos).get("id")+"");
            startActivity(intent);
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
        }
        return super.onOptionsItemSelected(item);
    }
/** reloads expandable list view and downloads new episodes */
    public void reload(){
        if(!isReloading){
            isReloading = true;
            getLists(loadFile());
            new DownloadEpisodes().execute(lista);
        }

    }


/** reloading info to expandable list view from file, without downloading new episodes */
    public void reloadOffline(){
        if(!isReloading){
            isReloading=true;
            setExpList((ArrayList<ArrayList<HashMap<String,Object>>>) jc.parseEpisodes(loadFile()),getLists(loadFile()));
            isReloading=false;
        }

    }

/** loads json from saved file */
    private String loadFile(){
        StringBuilder sb= new StringBuilder();
        try{
            FileInputStream fis =getActivity().openFileInput("series.json");
            InputStream is = fis;
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while((line=br.readLine())!=null){
                sb.append(line);
            }
            fis.close();
        }catch(Exception e){

        }
        return sb.toString();
    }

/** gets lists and set them to global variables  */
    public List<String> getLists(String seriesFile){
        ArrayList<HashMap<String,Object>> list = new ArrayList<>();
        List<String>headers = new ArrayList<>();
        lista = new ArrayList<>();

        try{
            list =(ArrayList<HashMap<String,Object>>) jc.seriesList(seriesFile);
        }catch(Exception e){
        }

        for(int i=0;i<list.size();i++){

            String a =(String) list.get(i).get("name");
            headers.add(a);
            HashMap<String,Integer> map = new HashMap<>();
            map.put("id",(int)list.get(i).get("id"));
            map.put("number_of_seasons",(int)list.get(i).get("number_of_seasons"));
            lista.add(map);
        }
        return headers;
    }
/** sets adapter to expandable list view */
    public void setExpList(ArrayList<ArrayList<HashMap<String,Object>>> all_series,List<String> headers){
        if(all_series.size()>0){
            MyExpandableListAdapter mela = new MyExpandableListAdapter(getActivity(),headers,all_series);
            exp_list_view.setAdapter(mela);

            exp_list_view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch(event.getAction() & MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_DOWN:
                            registerForContextMenu(v);
                            v.setPressed(true);
                            //tStart = System.currentTimeMillis();
                            break;
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_OUTSIDE:
                        case MotionEvent.ACTION_CANCEL:
                            v.setPressed(false);
                            break;
                        default: break;

                    }
                    return false;
                }
            });
        }

    }


 /** writes to file string from argument */
    public void writeFile(String toWrite){
        try{
            FileOutputStream fos = getActivity().openFileOutput("series.json",Context.MODE_PRIVATE);
            fos.write(toWrite.getBytes());
            fos.close();
        }catch (Exception e){

        }
    }

    public String readFile()throws Exception{
        StringBuilder sb = new StringBuilder();
        FileInputStream fis = getActivity().openFileInput("series.json");
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        String line;
        while((line=br.readLine())!=null){
            sb.append(line);
        }
        fis.close();
        return sb.toString();
    }

    public void deleteSerie(long position){
        Log.d("pozycja", (int)position+"");
        if(lista.size()>0){
            String serieId = ((int)lista.get((int)position).get("id"))+"";
            try{
                String toWrite = jc.deleteEpisode(readFile(),serieId);
                writeFile(toWrite);
                reloadOffline();
            }catch(Exception ex){

            }
        }


    }

    /** Downloads json file of episodes */
    class DownloadEpisodes extends AsyncTask<ArrayList<HashMap<String,Integer>>,String,ArrayList<ArrayList<HashMap<String,Object>>>> {
        JSONconverter jc = new JSONconverter();



        @Override
        protected void onPreExecute() {
            mProgressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected ArrayList<ArrayList<HashMap<String,Object>>> doInBackground(ArrayList<HashMap<String, Integer>>... params) {
            GregorianCalendar cal = new GregorianCalendar();

            ArrayList<ArrayList<HashMap<String,Object>>> all_series1 = new ArrayList<>();

            ArrayList<HashMap<String, Integer>> a = params[0];

            for(int e =0;e<a.size();e++){
                ArrayList<String> jsons = new ArrayList<>();
                ArrayList<HashMap<String,Object>> all_episodes2 =new ArrayList<>();
                for(int i=0;i<=a.get(e).get("number_of_seasons");i++){
                    StringBuilder sb = new StringBuilder();
                    try{
                        URL url = new URL(getResources().getString(R.string.search_serie)+a.get(e).get("id")+"/season/"+i+"?api_key="+getResources().getString(R.string.api_key));
                        HttpURLConnection con =(HttpURLConnection) url.openConnection();
                        con.connect();
                        InputStream is = con.getInputStream();
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));
                        String line;
                        while((line=br.readLine())!=null){
                            sb.append(line);
                        }
                        con.disconnect();

                    }catch(Exception ez){

                    }

                    jsons.add(sb.toString());
                }

                for(int i =0;i<jsons.size();i++){
                    ArrayList<HashMap<String,Object>> list =(ArrayList<HashMap<String,Object>>) jc.getEpisodes(jsons.get(i));
                    for(int y=0;y<list.size();y++){
                        HashMap<String,Object> map3 = list.get(y);
                        String name =(String) map3.get("name");
                        if(name.equals("")){
                            map3.put("name","N/A");
                        }
                        map3.put("serieId",a.get(e).get("id")) ;

                        String date = (String) map3.get("air_date");
                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                        Date epDate=null;

                        String year = cal.get(Calendar.YEAR)+"";

                        String month=null;
                        if(cal.get(Calendar.MONTH)<9){
                            month = "0"+(cal.get(Calendar.MONTH)+1);
                        }else{
                            month = (cal.get(Calendar.MONTH)+1)+"";
                        }
                        String day = null;
                        if(cal.get(Calendar.DAY_OF_MONTH)<10){
                            day="0"+cal.get(Calendar.DAY_OF_MONTH);
                        }else{
                            day= cal.get(Calendar.DAY_OF_MONTH)+"";
                        }

                        String td =year +"-"+month+"-"+day;

                        Date today=null;
                        //publishProgress(td);

                        try{
                            today =df.parse(td);
                            if(date==null) {
                                date = "1900-01-01";
                            }

                            epDate = df.parse(date);

                            if(epDate.after(today)||epDate.equals(today)){
                                all_episodes2.add(map3);
                            }

                        }catch(Exception ea){
                        }

                    }

                }
                if(all_episodes2.size()==0){
                    HashMap<String,Object> map =new HashMap<>();
                    map.put("name","There is no upcoming episodes");
                    map.put("serieId",a.get(e).get("id")+"");
                    map.put("air_date"," ");
                    all_episodes2.add(map);
                }
                all_series1.add(all_episodes2);
            }

            return all_series1;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(ArrayList<ArrayList<HashMap<String,Object>>> s) {
            //setExpList(s);
            putEpisodes(s);
            setExpList((ArrayList<ArrayList<HashMap<String,Object>>>) jc.parseEpisodes(loadFile()),getLists(loadFile()));
            mProgressBar.setVisibility(View.GONE);
            test.setText("Checked and downloaded for a new episodes!");
            test.show();
            isReloading = false;

        }



        private void putEpisodes(ArrayList<ArrayList<HashMap<String,Object>>> eps){
            String old=loadFile();
            for(int i=0;i<eps.size();i++){
                ArrayList<HashMap<String,Object>> serie = eps.get(i);
                JSONArray episodes_array = new JSONArray();

                //makes array to be put into json file
                for(int e =0;e<serie.size();e++){
                    HashMap<String,Object> episode = serie.get(e);

                    JSONObject epis = new JSONObject();
                    try{
                        epis.put("name",episode.get("name")+"");
                        epis.put("air_date",episode.get("air_date")+"");
                        epis.put("serieId",episode.get("serieId")+"");
                        episodes_array.put(epis);
                    }catch (JSONException ex){

                    }
                }
                try{
                    old = jc.utEpisodes(old,episodes_array);
                }catch(Exception ex){

                }


            }
            tx3.setText(old);
            writeFile(old);
        }
    }


    /** Expandable list adapter */
    class MyExpandableListAdapter extends BaseExpandableListAdapter{
        Context context;
        List<String> header_titles;
        ArrayList<ArrayList<HashMap<String,Object>>> episodes;

        int lastExpandedGroup;

        public MyExpandableListAdapter(Context cnt, List<String> ht, ArrayList<ArrayList<HashMap<String,Object>>> eps){
            this.context =cnt;
            this.header_titles =ht;
            this.episodes = eps;
        }
        @Override
        public void onGroupExpanded(int groupPosition) {
            if(groupPosition!=lastExpandedGroup){
                exp_list_view.collapseGroup(lastExpandedGroup);
            }

            super.onGroupExpanded(groupPosition);
            lastExpandedGroup = groupPosition;

        }

        @Override
        public int getGroupCount() {
            return header_titles.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return episodes.get(groupPosition).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return header_titles.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return episodes.get(groupPosition).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            if(getGroupCount()>0){
                for(int i=0;i<getGroupCount();i++){
                    if(getChildrenCount(i)==0){
                        reload();
                        break;
                    }
                }
            }



            String title = (String) getGroup(groupPosition);

            if(convertView==null){
                LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.exp_parent_layout,null);
            }

            TextView txTitle = (TextView)convertView.findViewById(R.id.heading_item);
            txTitle.setText(title);

            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


            HashMap<String,Object> map = (HashMap<String,Object>) this.getChild(groupPosition,childPosition);
            String title =(String) map.get("name");
            String air_date=(String) map.get("air_date");

            if(convertView==null){
                LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.exp_child_item,null);
            }
            TextView txTitle = (TextView)convertView.findViewById(R.id.child_title);
            TextView txAir = (TextView)convertView.findViewById(R.id.child_air_date);

            if(title==""){
                title = "No title yet";
            }
                txTitle.setText(title);
                txAir.setText(air_date);

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }



}
