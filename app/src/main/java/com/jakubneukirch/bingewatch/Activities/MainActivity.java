package com.jakubneukirch.bingewatch.Activities;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.SystemClock;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.ads.MobileAds;
import com.jakubneukirch.bingewatch.BackgroundService;
import com.jakubneukirch.bingewatch.NotificationPublisher;

import serieminder.com.seriale.R;

public class MainActivity extends AppCompatActivity {

    private static Context context;
    private Menu mMenu;
    public DrawerLayout mDrawerLayout;
    ListView mDrawerList;
    ActionBarDrawerToggle mDrawerToggle;
    String mTitle="";


    boolean searchOpened=false;

    WindowFragment wf = new WindowFragment();
    MySeriesFragment msf = new MySeriesFragment();
    AboutFragment af = new AboutFragment();
    FragmentManager fm = getFragmentManager();


    public static Context getContext() {
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_drawer_layout);
        context = getApplicationContext();
        configureNotifications();


        int fragment = getIntent().getIntExtra("fragment",0);


        changeFragment(fragment);

        mTitle = (String)getTitle();
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mDrawerList = (ListView)findViewById(R.id.drawer_list);


        mDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,null,R.string.open_drawer,R.string.close_drawer){
            @Override
            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                if(mMenu!=null){
                    mMenu.findItem(R.id.search_bara).setVisible(true);
                }
                getSupportActionBar().setTitle(mTitle);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(),R.layout.drawer_list_item,getResources().getStringArray(R.array.items));

        mDrawerList.setAdapter(adapter);

        getSupportActionBar().setHomeButtonEnabled(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String[] items = getResources().getStringArray(R.array.items);


                mTitle = items[position];
                changeFragment(position);

                mDrawerLayout.closeDrawer(mDrawerList);

            }
        });

        MobileAds.initialize(this,getResources().getString(R.string.native_ad_unit_id));


        invalidateOptionsMenu();
    }

    public void configureNotifications(){
        SharedPreferences settings = getSharedPreferences("PREFS",0);
        if(!settings.getBoolean("backgroundRunning",true)){
            Intent alarm = new Intent(context, NotificationPublisher.class);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            alarm.addCategory(NotificationPublisher.TAG);
            PendingIntent pi = PendingIntent.getBroadcast(context,1,alarm,PendingIntent.FLAG_UPDATE_CURRENT );
            alarmManager.cancel(pi);
            pi.cancel();
            stopService(alarm);
            //stopBgService();
        }

        if(!settings.getBoolean("backgroundRunning",true)){
            Log.d("backgroundRunning", "false");
            Intent alarm = new Intent(context, NotificationPublisher.class);
            alarm.addCategory(NotificationPublisher.TAG);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            PendingIntent pi = PendingIntent.getBroadcast(context,1,alarm,PendingIntent.FLAG_UPDATE_CURRENT );
            alarmManager.cancel(pi);
            pi.cancel();
            stopService(alarm);
           // stopBgService();


        }else{
            Log.d("backgroundRunning", "true");
            Intent alarm = new Intent(context, NotificationPublisher.class);
            alarm.addCategory(NotificationPublisher.TAG);
            boolean alarmRunning = (PendingIntent.getBroadcast(context, 1, alarm, PendingIntent.FLAG_NO_CREATE)!=null);

            if(alarmRunning==false){
                Log.d("Alarmrunning","false");

                AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
                PendingIntent pi = PendingIntent.getBroadcast(context,1,alarm,0 );
                alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(),AlarmManager.INTERVAL_HALF_DAY,pi);
            }else{
                Log.d("Alarmrunning","true");
            }


        }

    }

    private void stopBgService(){
        Intent intent = new Intent(this, BackgroundService.class);
        intent.addCategory(BackgroundService.TAG);
        stopService(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if(wf.getCheckAdapter()=="Search"&&(getFragmentManager().findFragmentByTag("SEARCH")!=null&&getFragmentManager().findFragmentByTag("SEARCH").isVisible())){
            wf.refreshAiring();
        }else{
            super.onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);


        final MenuItem searchItem = menu.findItem(R.id.search_bara);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(getFragmentManager().findFragmentByTag("SEARCH")!=null&&getFragmentManager().findFragmentByTag("SEARCH").isVisible()) {
                    wf.zapytanie = searchView.getQuery().toString();
                    wf.search();
                }else{
                    wf.zapytanie = searchView.getQuery().toString();
                    changeFragment(0);

                    wf.search();

                }

                searchView.clearFocus();
                return false;
            }



            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
         searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mMenu.findItem(R.id.more_my_series).setVisible(true);

            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                //mMenu.findItem(R.id.more_my_series).setVisible(true);
                invalidateOptionsMenu();

                return false;
            }
        });




        if(searchItem!=null){
            MenuItemCompat.setOnActionExpandListener(searchItem,new MenuItemCompat.OnActionExpandListener() {


                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    searchOpened=true;
                    invalidateOptionsMenu();
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    searchOpened=false;
                    if(wf.getCheckAdapter()=="Search"&&(getFragmentManager().findFragmentByTag("SEARCH")!=null&&getFragmentManager().findFragmentByTag("SEARCH").isVisible())) {
                        wf.refreshAiring();
                    }
                    invalidateOptionsMenu();
                    return true;
                }
            });
        }


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mDrawerToggle.onOptionsItemSelected(item)){

            return true;
        }
        switch(item.getItemId()){
            case R.id.refresh_series:
                if(getFragmentManager().findFragmentByTag("MY_SERIES")!=null&&getFragmentManager().findFragmentByTag("MY_SERIES").isVisible()){
                    msf.reload();
                }else if(getFragmentManager().findFragmentByTag("SEARCH")!=null&&getFragmentManager().findFragmentByTag("SEARCH").isVisible()){
                    //changeFragment(0);
                    wf.refreshAiring();
                    Log.d("menu","click");

                }
                 break;
            case R.id.menu_settings:
                Intent intent = new Intent(this,SettingsActivity.class);

                startActivity(intent);
                break;
            case R.id.search_bara: invalidateOptionsMenu();break;
        }
       return true;
        // return super.onOptionsItemSelected(item);
    }

    @Override public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mMenu = menu;

        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);

        menu.findItem(R.id.search_bara).setVisible(!drawerOpen);

        menu.findItem(R.id.more_my_series).setVisible(!searchOpened);
        Log.d("SEARCH",searchOpened+"");

       /* if(getFragmentManager().findFragmentByTag("MY_SERIES")!=null&& getFragmentManager().findFragmentByTag("MY_SERIES").isVisible()  ){

        }else{
        } */
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onResumeFragments() {
        invalidateOptionsMenu();
        super.onResumeFragments();
    }

    @Override
    protected void onStart() {
        super.onStart();
     /*   if(getFragmentManager().findFragmentByTag("SEARCH")!=null&&getFragmentManager().findFragmentByTag("SEARCH").isVisible()) {
            new Runnable() {
                @Override
                public void run() {
                    while (!wf.checkedLoad) {

                    }
                    if (wf.checkedLoad && !wf.nativeLoaded)
                        wf.loadInterstitial();
                }
            };
        } */
        Log.d("MainActivity","onstart");
    }



    @Override
    protected void onResume() {
        super.onResume();
        configureNotifications();
        Log.d("MainActivity","onResume()");
    }

    @Override
    protected void onStop() {
        super.onStop();


    }

    private boolean isServiceRunning(){
        ActivityManager manager = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
        for(ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)){
            Log.d("service is ==-===",service.service.getClassName());
            if("BackgroundService".equals(service.service.getClassName())){
                return  true;
            }
        }
        return false;
    }

    private void changeFragment(int pos){
        FragmentTransaction ft = fm.beginTransaction();
        switch(pos){
            case 0:
                if(getFragmentManager().findFragmentByTag("SEARCH")!=null&&getFragmentManager().findFragmentByTag("SEARCH").isVisible()){
                    ft.detach(wf);
                    ft.attach(wf);
                }


                ft.replace(R.id.content_frame, wf,"SEARCH");
                ft.commit();

                break;
            case 1:

                ft.replace(R.id.content_frame,msf,"MY_SERIES");
                ft.commit();
                break;
            case 2:
                ft.replace(R.id.content_frame,af,"ABOUT");
                ft.commit();
                break;
        }
        invalidateOptionsMenu();
    }
    public void changeFragment(int pos,String argument){
        FragmentTransaction ft = fm.beginTransaction();
        switch(pos){
            case 0:
                //WindowFragment wf = new WindowFragment();
                Bundle data = new Bundle();
                data.putString("query",argument);
                if(getFragmentManager().findFragmentByTag("SEARCH")!=null&&getFragmentManager().findFragmentByTag("SEARCH").isVisible()) {
                    ft.detach(wf);
                    ft.attach(wf);
                }
                wf.setArguments(data);
                ft.replace(R.id.content_frame, wf,"SEARCH");
                ft.commit();

                break;
            case 1:


                ft.replace(R.id.content_frame,msf,"MY_SERIES");
                ft.commit();
                break;
        }
        invalidateOptionsMenu();
    }

}
