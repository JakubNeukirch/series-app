package com.jakubneukirch.bingewatch.Activities;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.FloatRange;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.common.SignInButton;
import com.jakubneukirch.bingewatch.Activities.SerieActivity;
import com.jakubneukirch.bingewatch.JSONconverter;
import com.jakubneukirch.bingewatch.RateCanvas;

import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import serieminder.com.seriale.R;

/**
 * Created by Kuba on 2016-04-07.
 */
public class WindowFragment extends Fragment {

    private ListView lv;
    private String api_key;
    private String Link;
    String zapytanie = "Err404";
    private String jsonResult = "";
    private ProgressBar mProgressBar;
    View ad;
    private String checkAdapter = "Airing";
    private boolean isDownloadingJson = false;
    public boolean nativeLoaded = false;
    private int adCounter = 0;
    public boolean checkedLoad = false;
    private boolean firstLoad = false;

    NativeExpressAdView mAdView;
    InterstitialAd interestitialAd;

    JSONconverter jc = new JSONconverter();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MobileAds.initialize(getActivity(), getResources().getString(R.string.native_ad_unit_id));
        Link = getResources().getString(R.string.search_series_link);
        api_key = getResources().getString(R.string.api_key);
        //loadInterstitial();


        createNativeAd();

    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getActivity().setTitle("Airing Today");


        View v = inflater.inflate(R.layout.fragment_layout,container,false);
        lv=(ListView)v.findViewById(R.id.listView);
        mProgressBar=(ProgressBar)v.findViewById(R.id.progressBarSearch);

        try{
            zapytanie = getArguments().getString("query");
        }catch(Exception e){
            zapytanie = "Err404";
        }

        search();

        return v;
    }
    public void search(){
        if(zapytanie!="Err404"){
            zapytanie = zapytanie.replace(" ","+");
            try{
                new DownloadJSON().execute(Link+zapytanie+"&api_key="+api_key);
                zapytanie="Err404";
            }catch(Exception ex){

            }
        }

    }

    public void createNativeAd() {
        AdRequest adReq = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("0769D6C8514CABEB85C57BFB5387A6D1")
                .build();

        if (mAdView == null) {
            mAdView = new NativeExpressAdView(getActivity());

            mAdView.setAdSize(new AdSize(300, 250));
            mAdView.setAdUnitId(getResources().getString(R.string.native_ad_unit_id));
            //mAdView.setAdUnitId("Simulated error");
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);

                    nativeLoaded = false;
                    checkedLoad = true;
                    loadInterstitial();
                    adCounter = 1;
                    //((MyListAdapter)lv.getAdapter()).notifyDataSetChanged();
                    Log.d("NativeAd", "Failed to load");

                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    nativeLoaded = true;
                    checkedLoad = true;
                    adCounter = 1;
                    ((MyListAdapter) lv.getAdapter()).notifyDataSetChanged();
                    Log.d("NativeAd", "loaded");
                }
            });
            mAdView.loadAd(adReq);
           // ((MyListAdapter)lv.getAdapter()).notifyDataSetChanged();

        }
    }

    public String getCheckAdapter() {
        return checkAdapter;
    }

    private void setCheckAdapter(String checkAdapter) {
        this.checkAdapter = checkAdapter;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //ad=null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MobileAds.initialize(getActivity(),getResources().getString(R.string.native_ad_unit_id));

    }

    public boolean internetConnection(){
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if(networkInfo!=null&&networkInfo.isConnected()){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public void onPause() {
        if(mAdView!=null){
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().invalidateOptionsMenu();

      /*  if(zapytanie==null)
            zapytanie="Err404";
        if(lv.getAdapter()==null&&(zapytanie==null||zapytanie=="Err404")){*/

        if(mAdView!=null){
            mAdView.resume();
        }
        if(firstLoad&&nativeLoaded&&checkedLoad)
            loadInterstitial();


        if(firstLoad&&checkedLoad&&!nativeLoaded){
            loadInterstitial();

        }else{
            firstLoad =true;
        }



        if(lv.getAdapter()==null&&zapytanie=="Err404"&&isDownloadingJson==false)  {
            Log.d("list","called");
            refreshAiring();
        }

    Log.d("WindowFragment","onResume()");


    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.refresh_series).setVisible(true);
        menu.findItem(R.id.more_my_series).setVisible(true);
        getActivity().invalidateOptionsMenu();
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void setListView(ArrayList<HashMap<String,String>> list){


        if(list!=null){
            Log.d("list","not null");
            MyListAdapter adapter = new MyListAdapter(getActivity(),list);
            lv.setAdapter(null);
            lv.setAdapter(adapter);
            setCheckAdapter("Airing");
            lv.setOnItemClickListener(new OnAiringClick(list));
        }else{
            Log.d("list","null");
        }



    }

    class OnAiringClick implements AdapterView.OnItemClickListener{
        private ArrayList<HashMap<String,String>> jlist;
        public OnAiringClick(ArrayList<HashMap<String,String>> list) {
            this.jlist = list;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(getActivity(),SerieActivity.class);
            int pos = position;
            if(position>=3&&ad!=null)
                pos=position-1;

            intent.putExtra("id",jlist.get(pos).get("id"));
            startActivity(intent);
        }
    }

    class DownloadJSON extends AsyncTask<String,Void,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isDownloadingJson =true;
        }

        @Override
        protected String doInBackground(String... params) {
            StringBuilder sb = new StringBuilder();


                try {

                    URL url = new URL(params[0]);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();

                    con.connect();
                    InputStream is = new BufferedInputStream(con.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }

                } catch (Exception e) {
                    return "Connection Error";
                }


            return sb.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            final ArrayList<HashMap<String,String>> jsonlist;
            jsonResult = s;
            if(jsonResult=="Connection Error"){

            }
            JSONconverter jc = new JSONconverter();
            try{
                jsonlist =(ArrayList<HashMap<String,String>>) jc.searchSerie(jsonResult);
                if(jsonlist.size()>10){
                    for(int i=10;i<jsonlist.size();i++){
                        jsonlist.remove(i);
                    }
                }
                //ListAdapter adapter = new SimpleAdapter(getActivity(),jsonlist,R.layout.search_list_item,new String[]{"name"},new int[]{R.id.listText});
                SearchListAdapter adapter = new SearchListAdapter(getActivity(),jsonlist);
                lv.setAdapter(adapter);

                setCheckAdapter("Search");
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    ArrayList<HashMap<String,String>> jlist =jsonlist;
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getActivity(),SerieActivity.class);
                        intent.putExtra("id",jlist.get(position).get("id"));
                        refreshAiring();

                        startActivity(intent);


                    }
                });
            }catch(Exception e){

            }
            isDownloadingJson =false;
        }

    }


    public void refreshAiring(){
        //if(isAdded()){
            //new DownloadAiringToday().execute(getActivity().getResources().getString(R.string.airing_today)+getActivity().getResources().getString(R.string.api_key));
            createNativeAd();
            Log.d("list","refreshAiring()");
            DownloadAiringToday dat =  new DownloadAiringToday();
            dat.execute("http://api.themoviedb.org/3/tv/airing_today?api_key=948b102611ac966a27ec434331c4cc55");
       // }

    }

    public void showDialog(String title, String msg){
        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton("Refresh",new clickRefresh())
                .show();
        mAdView=null;
    }

    public void loadInterstitial(){
        Log.d("interstital","called");
        if(adCounter==0||adCounter%5==0){
            interestitialAd = new InterstitialAd(getActivity());
            interestitialAd.setAdUnitId(getResources().getString(R.string.interstitial_ad));
            AdRequest adReq = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .build();
            interestitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    interestitialAd.show();
                }
            });
            interestitialAd.loadAd(adReq);

        }
        adCounter++;
        Log.d("adCounter",adCounter+"");
    }

    class clickRefresh implements DialogInterface.OnClickListener{
        @Override
        public void onClick(DialogInterface dialog, int which) {
            refreshAiring();
        }
    }


    class DownloadAiringToday extends  AsyncTask<String, Void, String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
                mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            StringBuilder sb = new StringBuilder();

            try{
                URL url = new URL(params[0]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.connect();

                InputStream is = con.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String line;
                while((line=br.readLine())!=null){
                    sb.append(line);
                }
                return sb.toString();

            }catch (Exception ex){
                return "Error";
            }



        }

        @Override
        protected void onPostExecute(String s) {
            if(s!="Error"){
                ArrayList<HashMap<String,String>> list_airing = new ArrayList<>();
                try{
                    jc= new JSONconverter();
                    list_airing = jc.getAiringToday(s);

                    if(lv!=null)
                    {
                        setListView(list_airing);
                    }


                }catch(JSONException ex){

                }
            } else{
                showDialog("Error","Connection error...");
            }
                mProgressBar.setVisibility(View.GONE);
            super.onPostExecute(s);
        }
    }

    class SearchListAdapter extends ArrayAdapter<HashMap<String,String>>{
        Context context;
        Bitmap[] bmps;
        public SearchListAdapter(Context context, ArrayList<HashMap<String, String>> list) {
            super(context,R.layout.search_list_item ,list);
            this.context = context;
            int count = list.size();
            this.bmps = new Bitmap[count];
            new DownloadBmps().execute(list);
            /** hir  2 lines above*/
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = convertView;

            if(rowView==null){
                LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowView = inflater.inflate(R.layout.search_list_item,parent,false);
            }

            ImageView imgPoster = (ImageView) rowView.findViewById(R.id.imageView);
            TextView txTitle = (TextView) rowView.findViewById(R.id.listText);
            RelativeLayout rateDrawable =(RelativeLayout) rowView.findViewById(R.id.rateDrawableSearch);
            TextView txvVoteAvg = (TextView) rowView.findViewById(R.id.average_vote_search);

            HashMap<String,String> map = getItem(position);
            if(txTitle!=null){
                float vote = Float.parseFloat(map.get("vote_average"));
                txvVoteAvg.setText((String)map.get("vote_average"));

                float scale = 0.75f;
                txTitle.setText(map.get("name"));
                txTitle.setVisibility(View.VISIBLE);
                RateCanvas rateCanvas = new RateCanvas(getActivity(),14f*scale,14f*scale,72f*scale,72f*scale,24*scale,vote*36);
                rateDrawable.removeAllViews();
                rateDrawable.addView(rateCanvas);
                txvVoteAvg.bringToFront();
            }else{
                txTitle.setVisibility(View.INVISIBLE);
            }





            if(imgPoster!=null&&txTitle!=null){
                txTitle.setText(map.get("name"));
                txTitle.setVisibility(View.VISIBLE);
                //new DownloadBitmap(imgPoster).execute(getResources().getString(R.string.image_small_url)+map.get("poster_path"));
                if(bmps[position]!=null){
                    imgPoster.setImageBitmap(bmps[position]);
                }else{
                }
                imgPoster.setVisibility(View.VISIBLE);
            }else{
                txTitle.setVisibility(View.INVISIBLE);
                imgPoster.setVisibility(View.INVISIBLE);
            }


            return rowView;
        }
        class DownloadBmps extends AsyncTask<ArrayList<HashMap<String,String>>,Bitmap,Void>{
            Bitmap placeholder;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                BitmapFactory.Options options = new BitmapFactory.Options();

                options.inSampleSize = 2;

                options.inPreferredConfig = Bitmap.Config.RGB_565;
                //options.inDither = true;

                placeholder = BitmapFactory.decodeResource(context.getResources(),R.drawable.placeholder,options);
            }

            int w=0;
            @Override
            protected Void doInBackground(ArrayList<HashMap<String, String>>... params) {
                ArrayList<HashMap<String, String>> list = params[0];
                for(int i = 0;i<list.size();i++){
                    Bitmap bmp = null;
                    Log.d("DownloadBmps", list.get(i).get("poster_path")+" "+i );
                    if(!(list.get(i).get("poster_path")==null||list.get(i).get("poster_path")=="")){

                        try{
                            URL url = new URL(getResources().getString(R.string.image_small_url)+list.get(i).get("poster_path"));
                            HttpURLConnection con = (HttpURLConnection) url.openConnection();

                            con.connect();

                            BitmapFactory.Options opt = new BitmapFactory.Options();
                            opt.inPreferredConfig = Bitmap.Config.RGB_565;
                            opt.inSampleSize = 2;
                            InputStream is = con.getInputStream();
                            bmp = BitmapFactory.decodeStream(is,null,opt);
                            publishProgress(bmp);
                        }catch(Exception e){
                            publishProgress(placeholder);
                            Log.d("DownloadBmps", e.toString());
                        }
                    }else{
                        publishProgress(placeholder);
                    }

                }

                return null;
            }

            @Override
            protected void onProgressUpdate(Bitmap... values) {
                super.onProgressUpdate(values);
                if(values[0]!=null){
                    bmps[w] = values[0];
                } else{
                    bmps[w] = placeholder;
                }


                w++;
                notifyDataSetChanged();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

            }
        }

    }




    class MyListAdapter extends ArrayAdapter<HashMap<String,String>>{
        Bitmap[] bmps;
        Context context;

        Bitmap placeholder;


        MyListAdapter(Context context, ArrayList<HashMap<String,String>> list){

            super(context,R.layout.list_item_airing_today,list);
            this.context = context;
            bmps = new Bitmap[list.size()];
            new DownloadBmps().execute(list);
            placeholder = BitmapFactory.decodeResource(context.getResources(), R.drawable.placeholder);
        }



        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(context);

            View customView = inflater.inflate(R.layout.list_item_airing_today,parent,false);
            RelativeLayout serieAir =(RelativeLayout) customView.findViewById(R.id.serieAir);
            LinearLayout mainLay = (LinearLayout) customView.findViewById(R.id.mainLayout);
            RelativeLayout adContainer = (RelativeLayout) customView.findViewById(R.id.adContainer);
            RelativeLayout rateDrawable = (RelativeLayout) customView.findViewById(R.id.rateDrawable);

            if(position==2&&(internetConnection()||ad!=null)&&(checkedLoad&&nativeLoaded)){

                    if(ad!=null){
                        Log.d("AD","AD not null");
                        return ad;
                    }else{
                        Log.d("AD","making new add");
                        mainLay.setBackgroundColor(getResources().getColor(android.R.color.transparent));

                        serieAir.setVisibility(View.GONE);

                        adContainer.setVisibility(View.VISIBLE);
                        adContainer.setGravity(Gravity.CENTER);

                        adContainer.addView(mAdView);



                        ad=customView;
                    }

                }else {
                    //loadInterstitial();
                    int pos = position;
                    if(position>=3&&ad!=null){
                        pos= position-1;
                    }

                TextView txvTitle = (TextView) customView.findViewById(R.id.serieTitle);
                TextView txvOverview = (TextView) customView.findViewById(R.id.overview);
                TextView txvVoteAvg = (TextView) customView.findViewById(R.id.average_vote);
                ImageView imgPoster = (ImageView) customView.findViewById(R.id.poster);

                HashMap<String, String> map = getItem(pos);

                txvTitle.setText(map.get("name"));
                txvOverview.setText(map.get("overview"));
                txvVoteAvg.setText(map.get("vote_average"));

                float vote = Float.parseFloat(map.get("vote_average"));

                float scale = 0.7f;

                RateCanvas rateCanvas = new RateCanvas(getActivity(),13f*scale,13f*scale,72f*scale,72f*scale,26*scale,vote*36);
                rateDrawable.addView(rateCanvas);
                txvVoteAvg.bringToFront();


           /* HashMap<String,Object> downloadMap = new HashMap<>();
            downloadMap.put("url",getResources().getString(R.string.image_url)+map.get("poster_path"));
            downloadMap.put("imageview",imgPoster); */
                    if (imgPoster.getDrawable() == null) {
                        imgPoster.setImageBitmap(bmps[pos]);
                    } else {
                        imgPoster.setImageBitmap(placeholder);
                    }


                }


            return customView;
        }
        class DownloadBmps extends AsyncTask<ArrayList<HashMap<String,String>>,Bitmap,Void>{
            Bitmap placeholder;
            int w=0;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                BitmapFactory.Options options = new BitmapFactory.Options();

                options.inSampleSize = 2;

                options.inJustDecodeBounds = false;
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                options.inDither = true;

                placeholder = BitmapFactory.decodeResource(context.getResources(),R.drawable.placeholder,options);
            }

            @Override
            protected Void doInBackground(ArrayList<HashMap<String, String>>... params) {
                ArrayList<HashMap<String, String>> list = params[0];
                for(int i = 0;i<list.size();i++){
                    Bitmap bmp = null;
                    try{
                        URL url = new URL(getResources().getString(R.string.image_small_url)+list.get(i).get("poster_path"));
                        HttpURLConnection con = (HttpURLConnection) url.openConnection();

                        con.connect();

                        InputStream is = con.getInputStream();
                        bmp = BitmapFactory.decodeStream(is);
                        publishProgress(bmp);
                    }catch(Exception e){
                        publishProgress(placeholder);
                    }
                }

                return null;
            }

            @Override
            protected void onProgressUpdate(Bitmap... values) {
                super.onProgressUpdate(values);
                if(values[0]!=null)
                    bmps[w] = values[0];
                else{
                   // bmps[w] = placeholder;
                }
                w++;
                notifyDataSetChanged();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }




    }



}
