package com.jakubneukirch.bingewatch.Activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.jakubneukirch.bingewatch.BackgroundService;
import com.jakubneukirch.bingewatch.NotificationPublisher;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import serieminder.com.seriale.R;

/**
 * Created by 19ort on 25.04.2016.
 */
public class SettingsActivity extends AppCompatActivity {
    Context contextSuper = this;
    CheckBox cbx_background;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        SharedPreferences settings = getSharedPreferences("PREFS",0);
        final SharedPreferences.Editor editor_settings = settings.edit();
        cbx_background = (CheckBox)findViewById(R.id.cbx_background);
        cbx_background.setChecked(settings.getBoolean("backgroundRunning",true));

      /*  if(cbx_background.isChecked()){
            Intent intent = new Intent(this,BackgroundService.class);
            startService(intent);
        }else{
            Intent intent = new Intent(this,BackgroundService.class);
            stopService(intent);
        } */
        cbx_background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Context context = contextSuper;
                if(!cbx_background.isChecked()){
                    editor_settings.putBoolean("backgroundRunning",false);
                    Log.d("backgroundRunningSet", "false");
                    Intent alarm = new Intent(MainActivity.getContext(), NotificationPublisher.class);
                    alarm.addCategory(NotificationPublisher.TAG);
                    AlarmManager alarmManager = (AlarmManager) MainActivity.getContext().getSystemService(ALARM_SERVICE);
                    PendingIntent pi = PendingIntent.getBroadcast(MainActivity.getContext(),1,alarm,PendingIntent.FLAG_UPDATE_CURRENT );
                    alarmManager.cancel(pi);
                    pi.cancel();
                    stopService(alarm);
                    //stopBgService();



                }else{
                    Log.d("backgroundRunningSet", "true");
                    editor_settings.putBoolean("backgroundRunning",true);
                    Intent alarm = new Intent(MainActivity.getContext(), NotificationPublisher.class);
                    alarm.addCategory(NotificationPublisher.TAG);
                    boolean alarmRunning = (PendingIntent.getBroadcast(MainActivity.getContext(), 1, alarm, PendingIntent.FLAG_NO_CREATE)!=null);

                    if(alarmRunning==false){
                        Log.d("AlarmrunningSet","false");

                        AlarmManager alarmManager = (AlarmManager) MainActivity.getContext().getSystemService(ALARM_SERVICE);
                        PendingIntent pi = PendingIntent.getBroadcast(MainActivity.getContext(),1,alarm,0 );
                        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(),AlarmManager.INTERVAL_HALF_DAY,pi);
                    }else{
                        Log.d("AlarmrunningSet","true");

                    }
                }
                editor_settings.commit();
            }
        });

        TextView txtJSON = (TextView)findViewById(R.id.txtJSON);
        txtJSON.setText(loadFile());

        Button btnDel = (Button) findViewById(R.id.btnDel);
        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    File file = new File(getFilesDir(),"series.json");
                    file.delete();
                }catch(Exception ex){

                }

            }
        });

    }

    private void stopBgService(){
        Intent intent = new Intent(this, BackgroundService.class);
        intent.addCategory(BackgroundService.TAG);
        stopService(intent);

    }

    private String loadFile(){
        StringBuilder sb= new StringBuilder();
        try{
            FileInputStream fis =openFileInput("series.json");
            InputStream is = fis;
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while((line=br.readLine())!=null){
                sb.append(line);
            }
            fis.close();
        }catch(Exception e){

        }
        return sb.toString();
    }
}
