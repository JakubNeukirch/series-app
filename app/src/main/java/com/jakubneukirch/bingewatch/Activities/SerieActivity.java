package com.jakubneukirch.bingewatch.Activities;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.jakubneukirch.bingewatch.Download.DownloadSerieEpisodes;
import com.jakubneukirch.bingewatch.JSONconverter;
import com.jakubneukirch.bingewatch.RateCanvas;
import com.jakubneukirch.bingewatch.WidgetFiles.WidgetService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import serieminder.com.seriale.R;

/**
 * Created by Kuba on 2016-04-10.
 */
public class SerieActivity extends AppCompatActivity {

    private String Link;
    private String api_key;
    private String id;
    private String poster_path;
    private int nums;
    private ProgressBar mProgressBar;
    private int progressStatus=0;

    private boolean isDescOpen=false;

    ImageView imagePoster;
    TextView textTitle;
    TextView textDesc;
    ExpandableListView exp;
    Button btnAdd;
    DownloadJSON downloadJSON;
    JSONconverter jc;
    RelativeLayout rateDrawable;
    TextView txvAverage;

    String title;
    String desc;

    Toast test;

    Bitmap posterBmp;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.serie_fragment_layout);

        getSupportActionBar().setHomeButtonEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        test = Toast.makeText(getApplicationContext(),"a",Toast.LENGTH_SHORT);

        id = getIntent().getStringExtra("id");

        Link = getResources().getString(R.string.search_serie);
        api_key = getResources().getString(R.string.api_key);

        textTitle = (TextView)findViewById(R.id.textTitle);
        textDesc = (TextView)findViewById(R.id.textDesc);
        imagePoster = (ImageView)findViewById(R.id.imagePoster);
        mProgressBar = (ProgressBar)findViewById(R.id.progressBarSerie);
        rateDrawable = (RelativeLayout) findViewById(R.id.rateDrawableSerie);
        txvAverage = (TextView) findViewById(R.id.average_vote_serie);

        exp = (ExpandableListView)findViewById(R.id.expandableListView);
        btnAdd = (Button)findViewById(R.id.buttonAdd);

        checkButton();

        jc = new JSONconverter();

        btnAdd.setVisibility(View.INVISIBLE);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isDuplicated()){
                    outputToFile();
                }else{
                    deleteSerie();
                    updateWidget();
                }


            }
        });

        textDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isDescOpen==false){
                    textDesc.setText(desc);
                    isDescOpen=true;
                }

                else {
                    textDesc.setText(desc.substring(0,100)+"...");
                    isDescOpen=false;
                }

            }
        });

        downloadJSON = new DownloadJSON();
        downloadJSON.execute(Link+id+"?api_key="+api_key);
    }

    public void deleteSerie(){
            try{
                String toWrite = jc.deleteEpisode(readFile(),id);
                writeFile(toWrite);

                checkButton();
            }catch(Exception ex){

            }

    }

    private void checkButton(){
        if(isDuplicated()){
            btnAdd.setText("-");
        }else{
            btnAdd.setText("+");
        }
    }

    //sets all into layout
    public void setInfo(){
        JSONconverter jc = new JSONconverter();
        ArrayList<HashMap<String,Object>> jsonlist;
        jsonlist =(ArrayList<HashMap<String,Object>>) jc.searchSerieInfo(downloadJSON.getJSON());
        if(jsonlist!=null&&jsonlist.size()!=0) {
            title = (String) jsonlist.get(0).get("name");
            desc = (String) jsonlist.get(0).get("desc");
            poster_path = (String) jsonlist.get(0).get("poster_path");
            nums = (int) jsonlist.get(0).get("number_of_seasons");
            txvAverage.setText((String)jsonlist.get(0).get("vote_average"));

            float vote =  Float.parseFloat(jsonlist.get(0).get("vote_average")+"");
            float scale = 1.0f;

            RateCanvas rateCanvas = new RateCanvas(this,12.5f*scale,12.5f*scale,72.5f*scale,72.5f*scale,25*scale,vote*36);
            rateDrawable.addView(rateCanvas);
            txvAverage.bringToFront();

            textTitle.setText(title);
            if (desc.length() > 100) {
                textDesc.setText(desc.substring(0, 100) + "...");
            } else {
                textDesc.setText(desc);
            }

            getSupportActionBar().setTitle(title);

            new DownloadBitmap().execute("https://image.tmdb.org/t/p/w780/" + poster_path);
            String[] titles = new String[nums];
            String[] urls = new String[titles.length];
            for (int i = 0; i < titles.length; i++) {
                titles[i] = "Season " + (i + 1);
                int o = i + 1;
                urls[i] = "http://api.themoviedb.org/3/tv/" + id + "/season/" + o + "?api_key=" + api_key;

                // new DownloadJSONepisodes().execute("http://api.themoviedb.org/3/tv/1407/season/"+(i+1)+"?api_key="+api_key);

            }
            new DownloadJSONepisodes().execute(urls);
        }else{
            onBackPressed();
            Toast noInternet = Toast.makeText(this,"Internet Connection Error",Toast.LENGTH_LONG);
            noInternet.show();
        }
        btnAdd.setVisibility(View.VISIBLE);



    }

    public void updateWidget(){
        Intent updateWidget = new Intent(this, WidgetService.class);
        updateWidget.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int[] ids = {R.xml.widget_provider};
        updateWidget.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
        sendBroadcast(updateWidget);
       /* RemoteViews remoteViews = new RemoteViews(this.getPackageName(),R.layout.activity_widget);
        ComponentName thisWidget = new ComponentName(this, WidgetService.class);
        AppWidgetManager.getInstance(this).updateAppWidget(thisWidget,remoteViews); */
    }

    private void checkProgress(){
        if(progressStatus!=0){
            mProgressBar.setVisibility(View.VISIBLE);
        }else{
            mProgressBar.setVisibility(View.GONE);
        }
    }

    public void setEpisodes(String[] jsons){

        JSONconverter jc =new JSONconverter();

        //TO DAĆ JAKO ODCINKI
        ArrayList<ArrayList<HashMap<String,Object>>> seasons = new ArrayList<>();
        ArrayList<String[]> lista = new ArrayList<>();

        String[] head =new String[jsons.length];
        for(int i=0;i<jsons.length;i++){


            //wczytuje dane odcinka
            HashMap<String,Object> map = new HashMap<>();

            ArrayList<HashMap<String,Object>> episodes =(ArrayList<HashMap<String,Object>>) jc.getEpisodes(jsons[i]);
            if(episodes.size()!=0&&episodes!=null) {


                String[] array = new String[(int) episodes.get(0).get("number_of_episodes")];
                for (int y = 0; y < array.length; y++) {
                    array[y] = (y + 1) + (String) episodes.get(y).get("name");
                }
                head[i] = "Season " + (i + 1);

                seasons.add(episodes);

                lista.add(array);
            }
        }



        //ArrayList<HashMap<String,Object>> list =(ArrayList<HashMap<String,Object>>) jc.getEpisodes(jsons[0]);

        List<String> headings = new ArrayList<String>();

        for(String title : head){
            headings.add(title);
        }



        ExpListAdapter expListAdapter = new ExpListAdapter(this,headings,seasons );
        expListAdapter.onlyGroupHeight();
        exp.setAdapter(expListAdapter);

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.serie_menu,menu);
        return true;
        //return super.onCreateOptionsMenu(menu);
    }

    public boolean isDuplicated(){
        JSONconverter jc =new JSONconverter();
        ArrayList<HashMap<String,Object>> list = new ArrayList<>();
        try{
            list = (ArrayList<HashMap<String,Object>>)jc.seriesList(getFile());

        }catch(Exception e){

        }

        for(int i=0;i<list.size();i++){
            String a =list.get(i).get("id")+"";
            if(a.equals(id)){

                return true;
            }
        }
        return false;

    }

    private boolean fileExists(){
        try{
            FileInputStream fis = openFileInput("series.json");
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line = "";
            while((line=br.readLine())!=null){

            }
            fis.close();
            return true;
        }catch (Exception ex){
            return false;
        }

    }

    public void outputToFile(){

        if(!fileExists()){
            try{
                Log.d("otp","1");
                createFile();

            }catch(Exception e){

            }

        }

            if(!isDuplicated()){
                    try{
                        add();
                        test.setText("Serie added to your favorites!");
                        test.show();


                    }catch(Exception e){
                        Log.d("SerieActivity", e.toString());
                        try{
                            //creating new file
                            FileOutputStream fos = openFileOutput("series.json",MODE_PRIVATE);
                            JSONObject jo = new JSONObject();
                            JSONArray ja =new JSONArray();
                            jo.put("series",ja);

                            fos.write(jo.toString().getBytes());
                            fos.close();

                            //adding
                            try{
                                add();
                                test.setText("Serie added to your favorites!");
                                Log.d("hir","im");
                                test.show();
                            }catch(Exception ex){
                                Log.d("SerieActivity", ex.toString());
                                test.setText("Something went wrong while adding!");
                                test.show();
                            }




                        }catch(Exception x){
                            Log.d("SerieActivity", x.toString());
                        }
                    }

            } else{
                test.setText("This serie is already in your favorites!");
                test.show();
            }


        }
    private void createFile()throws Exception{
            Log.d("SerieActivity","creating new file");
            FileOutputStream fos = openFileOutput("series.json",MODE_PRIVATE);
            JSONObject jo = new JSONObject();
            JSONArray ja =new JSONArray();
            jo.put("series",ja);

            fos.write(jo.toString().getBytes());
            fos.close();


    }

    //}

    private String readFile(){
        StringBuilder sb = new StringBuilder();
        try{
            FileInputStream fis = openFileInput("series.json");
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line;
            while((line=br.readLine())!=null){
                sb.append(line);
            }
            fis.close();
        }catch(Exception ex){

        }
        return sb.toString();
    }

    private void writeFile(String toWrite){
            try{
                FileOutputStream fos = openFileOutput("series.json", MODE_PRIVATE);
                fos.write(toWrite.getBytes());
                fos.close();
            }catch(Exception e){

            }

    }

    public void add()throws Exception{
       /* StringBuilder sb = new StringBuilder();
        String toWrite="";
        JSONconverter jc = new JSONconverter();

        String old = sb.toString();
        toWrite = jc.putSeriesList(old,title,id,nums);
        FileOutputStream fos = openFileOutput("series.json",MODE_PRIVATE);
        fos.write(toWrite.getBytes());
        fos.close();*/
        WriteFile wf = new WriteFile(this);
        wf.execute();
            //test.setText("Serie added to favorites!");
           // test.show();



    }

    class WriteFile extends AsyncTask<Void,Void,String>{

        DownloadSerieEpisodes dse = new DownloadSerieEpisodes();
        private Context context;

        public WriteFile(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            HashMap<String,Object> map = new HashMap<>();
            map.put("id",id+"");
            map.put("number_of_seasons",nums);
            map.put("context",context);


            dse.execute(map);

        }

        @Override
        protected String doInBackground(Void... params) {
            JSONconverter jc = new JSONconverter();
            // String s =text;
            StringBuilder sb = new StringBuilder();
            String toWrite="";
            try{
                if(!fileExists()){
                    try{
                        Log.d("dib","2");
                        createFile();

                    }catch(Exception e){
                        Log.d("SerieActivity1",e.toString());
                    }

                }
                FileInputStream fis = openFileInput("series.json");
                BufferedReader br = new BufferedReader(new InputStreamReader(fis));
                String line;
                while((line=br.readLine())!=null){
                    sb.append(line);
                }
                fis.close();
                String old = sb.toString();

                while(!dse.isDownloaded()){
                    Log.d("SerieActivity",dse.isDownloaded()+"");
                }

                toWrite = jc.putSeriesList(old,title,id,nums,dse.getEpisodes());
                Log.d("SerieActivity",toWrite.length()+"");
            }catch(Exception e){
                Log.d("SerieActivity2",e.toString());
            }

            return toWrite;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try{
                FileOutputStream fos = openFileOutput("series.json",MODE_PRIVATE);
                fos.write(s.getBytes());
                fos.close();
            }catch (Exception e){
                Log.d("SerieActivity", e.toString());
            }
            checkButton();
            updateWidget();

        }
    }

    private String getFile(){
        StringBuilder sb = new StringBuilder();
        if(!fileExists()){
            try{
                Log.d("gf","3");
                createFile();
            }catch (Exception e){

            }

        }
        try{
            FileInputStream fis = openFileInput("series.json");
            try{
                BufferedReader br = new BufferedReader(new InputStreamReader(fis));
                String line;

                while((line = br.readLine())!=null)
                    sb.append(line);

                fis.close();
            }catch(IOException ex){
                Log.d("SerieActivity ex",ex.toString());

            }


        }catch(FileNotFoundException e){
            Log.d("SerieActivity e",e.toString());
        }

        return sb.toString();
    }


    //Downloads the main JSON file
    class DownloadJSON extends AsyncTask<String,Void,String> {
        String JSONraw;
        StringBuilder sb = new StringBuilder();
        @Override
        protected String doInBackground(String... params) {
            try{
                URL url = new URL(params[0]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.connect();
                InputStream is = new BufferedInputStream(con.getInputStream());
                BufferedReader r = new BufferedReader(new InputStreamReader(is));
                String line ="";
                while((line=r.readLine())!=null){
                    sb.append(line);
                }


            }catch(Exception e){
                return "Connection Error";
            }

            return sb.toString();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressStatus++;
            checkProgress();
        }

        @Override
        protected void onPostExecute(String s) {

           JSONraw = s;
            setInfo();
            progressStatus--;
            checkProgress();
        }

        public String getJSON(){

            return JSONraw;
        }

    }

    //Downloads Season episodes
    class DownloadJSONepisodes extends AsyncTask<String,Void,String[]>{
        @Override
        protected String[] doInBackground(String... params) {
            String[] jsony = new String[params.length];

            for(int y=0;y<params.length;y++){
                StringBuilder sb = new StringBuilder();
                try{
                    URL url = new URL(params[y]);
                    HttpURLConnection con = (HttpURLConnection)url.openConnection();
                    con.connect();
                    InputStream is = new BufferedInputStream(con.getInputStream());
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while((line=br.readLine())!=null){
                        sb.append(line);
                    }
                    jsony[y]=sb.toString();

                }catch(Exception e){

                }
            }


            return jsony;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressStatus++;
            checkProgress();
        }

        @Override
        protected void onPostExecute(String[] s) {
            setEpisodes(s);
            progressStatus--;
            checkProgress();
        }

    }



    //Downloads the Bitmap
    class DownloadBitmap extends AsyncTask <String,Void,Bitmap>{
        @Override
        protected Bitmap doInBackground(String... params) {
            try{
                URL url = new URL(params[0]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();

                con.setDoInput(true);
                con.connect();

                InputStream is = con.getInputStream();
                Bitmap bmp = BitmapFactory.decodeStream(is);
                return bmp;
            }catch(Exception e){
                return  null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressStatus++;
            checkProgress();
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            posterBmp = bitmap;
            if(posterBmp!=null){
                imagePoster.setImageBitmap(posterBmp);
            }
            progressStatus--;
            checkProgress();

            //super.onPostExecute(bitmap);
        }
    }

    //Adapter for expandablelistview
    public class ExpListAdapter extends BaseExpandableListAdapter{

        private int expandedGroup;
        private int lastExpandedGroup;

        private List<String> header_titles;
        private ArrayList<ArrayList<HashMap<String,Object>>> child_titles;
        private Context context;

        public ExpListAdapter(Context cnt,List<String> hd,ArrayList<ArrayList<HashMap<String,Object>>> child){
            this.header_titles = hd;
            this.context = cnt;
            this.child_titles = child;
        }



        @Override
        public int getGroupCount() {
            return header_titles.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return child_titles.get(groupPosition).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return header_titles.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return child_titles.get(groupPosition).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            String title =(String) this.getGroup(groupPosition);
            if(convertView==null){
                LayoutInflater layoutInflater =(LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.exp_parent_layout,null);
            }

            TextView txv =(TextView) convertView.findViewById(R.id.heading_item);
            txv.setTypeface(null, Typeface.BOLD);
            txv.setText(title);

            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

            HashMap<String,Object> map =(HashMap<String, Object>) this.getChild(groupPosition,childPosition);
            if(convertView==null){
                LayoutInflater layoutInflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.exp_child_item,null);
            }

            TextView txvTitle = (TextView)convertView.findViewById(R.id.child_title);
            TextView txvAirDate = (TextView)convertView.findViewById(R.id.child_air_date);

            String title = (String) map.get("name");
            String air_date = (String)map.get("air_date");

            txvTitle.setText(title);
            txvAirDate.setText(air_date);

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
            if(groupPosition!=lastExpandedGroup){
                exp.collapseGroup(lastExpandedGroup);
            }

            super.onGroupExpanded(groupPosition);
            lastExpandedGroup = groupPosition;
            changeHeight();

        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
            if(groupPosition==lastExpandedGroup){
                onlyGroupHeight();
            }


            super.onGroupCollapsed(groupPosition);
        }

        private void changeHeight(){
            int m=0;

            m += 70;

            m += getChildrenCount(lastExpandedGroup) * 51 ;

            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) exp.getLayoutParams();
            lp.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,(float)m,getResources().getDisplayMetrics());
        }

        public void onlyGroupHeight(){
            int m =0;
            m= getGroupCount()*39;


            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) exp.getLayoutParams();
            lp.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,(float)m,getResources().getDisplayMetrics());

        }
    }


}

