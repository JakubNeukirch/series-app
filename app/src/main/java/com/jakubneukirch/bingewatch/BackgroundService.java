package com.jakubneukirch.bingewatch;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.jakubneukirch.bingewatch.Activities.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;

import serieminder.com.seriale.R;


/**
 * Created by 19ort on 24.04.2016.
 */
public class BackgroundService extends Service{
    private Context context;
    private boolean isRunning;
    private Thread backgroundThread;
    public static final String TAG = "BackgroundServiceTag";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        this.context = this;
        this.isRunning = false;
        this.backgroundThread = new Thread(myTask);
    }



    private Runnable myTask = new Runnable() {
        @Override
        public void run() {

            LoadFile lf = new LoadFile(context);
            ArrayList<HashMap<String,Object>> eps = lf.returnTodayEpisodes();
            Log.d("eps",eps.size()+"");

            Intent intent = new Intent(context, MainActivity.class);
            PendingIntent pi = PendingIntent.getActivity(context,0,intent,0);

            Log.d("NOTIFICATION","push");
            NotificationCompat.Builder nBuilder = new NotificationCompat.Builder(context);
            nBuilder.setSmallIcon(R.drawable.notification);
            nBuilder.setContentTitle("Your today episodes:");
            nBuilder.setContentIntent(pi);
            nBuilder.setAutoCancel(true);


            if(eps!=null&&eps.size()>0){
                StringBuilder sb = new StringBuilder();
                for(int i=0;i<eps.size();i++){
                    sb.append(eps.get(i).get("serie")+ " - " + eps.get(i).get("name")+"\n");
                }

                nBuilder.setContentText(eps.get(0).get("serie")+ " - " + eps.get(0).get("name"));
                nBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(sb.toString()));
                Notification not = nBuilder.build();

                NotificationManagerCompat.from(context).notify(0,not);
            }else{
                /*nBuilder.setContentText("No episodes today.");
                Notification not = nBuilder.build();

                NotificationManagerCompat.from(context).notify(0,not);*/
            }

            stopSelf();
        }
    };

    @Override
    public void onDestroy() {
        this.isRunning = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
            if(!isRunning){
                this.isRunning = true;
                this.backgroundThread.start();
            }
        return START_STICKY;
    }
}