package com.jakubneukirch.bingewatch.Download;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.jakubneukirch.bingewatch.JSONconverter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.Exchanger;

import serieminder.com.seriale.R;

/**
 * Created by 19ort on 28.07.2016.
 */
public class DownloadSerieEpisodes extends AsyncTask<HashMap<String,Object>,Void,JSONArray>{

    private JSONconverter jc = new JSONconverter();
    private JSONArray episodes;
    private boolean isDownloaded = false;
    private Context context;

    public boolean isDownloaded() {
        return isDownloaded;
    }

    private void setDownloaded(boolean downloaded) {
        isDownloaded = downloaded;
    }



    public JSONArray getEpisodes() {
        return episodes;
    }



    @Override
    protected void onPreExecute() {
        setDownloaded(false);
        episodes = new JSONArray();

        super.onPreExecute();
    }

   /* @Override
    protected JSONArray doInBackground(HashMap<String,Object>... params) {
        Calendar calToday = Calendar.getInstance();
        StringBuilder sb = new StringBuilder();
        ArrayList<String> seasons = new ArrayList<>();
        this.context = (Context)params[0].get("context");
        Log.d("des6",((int)params[0].get("number_of_seasons"))+"");
        for(int i=0;i<=(int)params[0].get("number_of_seasons");i++){


            try{
                URL url = new URL(context.getResources().getString(R.string.search_serie)+params[0].get("id")+"/season/"+i+"?api_key="+context.getResources().getString(R.string.api_key));
                HttpURLConnection con =(HttpURLConnection)url.openConnection();
                con.connect();
                InputStream is = con.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String line="";
                while((line=br.readLine())!=null){
                    sb.append(line);
                }
                seasons.add(sb.toString());
            }catch (Exception e){
                Log.d("DownloadSerieEpisodes1",e.toString());
            }

        }


        ArrayList<HashMap<String,Object>> out = new ArrayList<>();

        for(int i = 0;i < seasons.size();i++){
            try{
                ArrayList<HashMap<String,Object>> jsonlist = (ArrayList<HashMap<String,Object>>) jc.getEpisodes(seasons.get(i)) ;
                Log.d("DownloadSerieEpisodes",jsonlist.size()+"");
                Log.d("DownloadSerieEpisodes",jsonlist.get(i).get("name")+"");
                Log.d("DownloadSerieEpisodes",jsonlist.get(i).get("id")+"");
                Log.d("DownloadSerieEpisodes",jsonlist.get(i).get("serieId")+"");
                for(int e = 0; e<jsonlist.size();e++){
                    out.add(jsonlist.get(e));
                }
            }catch (Exception e){
                Log.d("DownloadSerieEpisodes2",e.toString());
            }


        }
        JSONArray episodes = new JSONArray();
        int upcEpisodes = 0;
        Log.d("DES8", out.size()+"");
        for(int i = 0; i < out.size();i++){
            try{

                JSONObject obj = new JSONObject();
                String episodeDate = out.get(i).get("air_date")+"";
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date epDate = sdf.parse(episodeDate);
                Calendar epCal = Calendar.getInstance();
                epCal.setTime(epDate);

                //Log.d("DES7",calToday.get(Calendar.YEAR) +":"+ calToday.get(Calendar.DAY_OF_YEAR) + " -||- " + epCal.get(Calendar.YEAR) +":"+epCal.get(Calendar.DAY_OF_YEAR)+" -- "+ i);
                if((calToday.get(Calendar.YEAR)==epCal.get(Calendar.YEAR)&&calToday.get(Calendar.DAY_OF_YEAR)==epCal.get(Calendar.DAY_OF_YEAR))||(calToday.get(Calendar.YEAR)<epCal.get(Calendar.YEAR)&&calToday.get(Calendar.DAY_OF_YEAR)==epCal.get(Calendar.DAY_OF_YEAR))){
               // if(calToday.equals(epCal)||calToday.before(epCal)){
                    obj.put("name",out.get(i).get("name")+"");
                    obj.put("air_date",out.get(i).get("air_date")+"");
                    obj.put("id",out.get(i).get("id")+"");
                    obj.put("overview",out.get(i).get("overview")+"");
                    obj.put("season_number",out.get(i).get("season_number")+"");
                    obj.put("number_of_episodes",out.get(i).get("number_of_episodes")+"");
                    obj.put("SerieId",params[0].get("id")+"");
                    episodes.put(obj);
                    upcEpisodes++;
                    Log.d("DownloadSerieEpisodes","added "+upcEpisodes);
                }

            }catch (Exception e){
                Log.d("DownloadSerieEpisodes4",e.toString());
            }

        }
        Log.d("DownloadSerieEpisodes",upcEpisodes+"");
        if(upcEpisodes==0){

            JSONObject obj = new JSONObject();
            try{
                obj.put("name","No upcoming episodes");
                obj.put("air_date","N/A");
                obj.put("id",out.get(0).get("id")+"");
                episodes.put(obj);
            }catch (Exception e){
                Log.d("DownloadSerieEpisodes5",e.toString());
            }

        }

        return episodes;
    } */
    @Override
    protected JSONArray doInBackground(HashMap<String,Object>... params) {
        this.context = (Context)params[0].get("context");
        Calendar cal =Calendar.getInstance();
        ArrayList<String> jsons = new ArrayList<>();
        JSONArray all_episodes2 =new JSONArray();
        for(int i=0;i<=(int)params[0].get("number_of_seasons");i++){
            StringBuilder sb = new StringBuilder();
            try{
                URL url = new URL(context.getResources().getString(R.string.search_serie)+params[0].get("id")+"/season/"+i+"?api_key="+context.getResources().getString(R.string.api_key));
                HttpURLConnection con =(HttpURLConnection) url.openConnection();
                con.connect();
                InputStream is = con.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String line;
                while((line=br.readLine())!=null){
                    sb.append(line);
                }
                con.disconnect();

            }catch(Exception ez){

            }

            jsons.add(sb.toString());
        }
        int upcEps = 0;
        for(int i =0;i<jsons.size();i++){
            ArrayList<HashMap<String,Object>> list =(ArrayList<HashMap<String,Object>>) jc.getEpisodes(jsons.get(i));
            for(int y=0;y<list.size();y++){
                JSONObject obj = new JSONObject();
                HashMap<String,Object> map3 = list.get(y);
                String name =(String) map3.get("name");
                if(name.equals("")){
                    map3.put("name","N/A");
                }
                map3.put("serieId",params[0].get("id")) ;

                String date = (String) map3.get("air_date");
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

                Date epDate=null;

                String year = cal.get(Calendar.YEAR)+"";

                String month=null;
                if(cal.get(Calendar.MONTH)<9){
                    month = "0"+(cal.get(Calendar.MONTH)+1);
                }else{
                    month = (cal.get(Calendar.MONTH)+1)+"";
                }
                String day = null;
                if(cal.get(Calendar.DAY_OF_MONTH)<10){
                    day="0"+cal.get(Calendar.DAY_OF_MONTH);
                }else{
                    day= cal.get(Calendar.DAY_OF_MONTH)+"";
                }

                String td =year +"-"+month+"-"+day;

                Date today=null;
                //publishProgress(td);

                try{
                    today =df.parse(td);
                    if(date==null) {
                        date = "1900-01-01";
                    }

                    epDate = df.parse(date);

                    if(epDate.after(today)||epDate.equals(today)){

                        obj.put("name",map3.get("name"));
                        obj.put("air_date", map3.get("air_date"));
                        obj.put("seriesId",params[0].get("id"));
                        all_episodes2.put(obj);
                        upcEps++;
                    }

                }catch(Exception ea){
                }

            }

        }
        if(upcEps==0){
            JSONObject obj = new JSONObject();
            try{
                obj.put("name","There is no upcoming episodes");
                obj.put("serieId",params[0].get("id")+"");
                obj.put("air_date"," ");
            }catch(Exception ex){

            }

            all_episodes2.put(obj);
        }

        return all_episodes2;
    }



    @Override
    protected void onPostExecute(JSONArray s) {
        super.onPostExecute(s);
        this.episodes = s;

        setDownloaded(true);
        Log.d("DownloadSerieEpisodes","downloaded");
    }
}
