package com.jakubneukirch.bingewatch;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kuba on 2016-04-09.
 */
public class JSONconverter {

    public ArrayList<ArrayList<HashMap<String,Object>>> getFileSeries(Context context)throws FileNotFoundException{ /** odbieranie zawartosci pliku*/
        StringBuilder sb = new StringBuilder();
        Log.d("context",context.toString());
        FileInputStream fis = context.openFileInput("series.json");
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        try {


            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                Log.d("line",line);
            }
            fis.close();
        }catch (IOException ex){

        }



        return parseEpisodes(sb.toString());


    }

    public Date getTodayDate(){
        GregorianCalendar cal = new GregorianCalendar();
        Date date = new Date();
        date.setTime(System.currentTimeMillis());

        Date today = new Date();
        today.setYear(cal.get(Calendar.YEAR));
        today.setMonth(cal.get(Calendar.MONTH));
        today.setDate(cal.get(Calendar.DAY_OF_MONTH));

        return today;
    }

    public String deleteEpisode(String all, String serieId)throws Exception{
        JSONObject retObj = new JSONObject();

        JSONObject main_object = new JSONObject(all);
        JSONArray series_array = main_object.getJSONArray("series");

        for(int i=0;i<series_array.length();i++){
            JSONObject serie_object =(JSONObject) series_array.get(i);
            Log.d("idObj",serie_object.get("id")+"");
            Log.d("argId",serieId);
            if((serie_object.get("id")+"").equals(serieId)){
                JSONArray newArray = new JSONArray();
                for (int e=0;e<series_array.length();e++){
                    if(e!=i){
                        newArray.put(series_array.get(e));
                        Log.d("eeeee",e+"");
                    }
                }
                retObj.put("series",newArray);
                break;
            }
        }
        return retObj.toString();
    }

    public  ArrayList<HashMap<String,String>> getAiringToday(String json)throws JSONException{

        ArrayList<HashMap<String,String>> list = new ArrayList<>();

        JSONObject main_object = new JSONObject(json);
        JSONArray array_results = main_object.getJSONArray("results");

        for(int i=0;i<array_results.length();i++){
            JSONObject object_result = array_results.getJSONObject(i);

            HashMap<String,String> map = new HashMap<>();

            String name = object_result.getString("name");
            String id = object_result.getString("id");
            String poster_path = object_result.getString("poster_path");
            String overview = object_result.getString("overview");
            String vote_average = object_result.getString("vote_average");

            map.put("name",name);
            map.put("id",id);
            map.put("poster_path",poster_path);
            map.put("overview",overview);
            map.put("vote_average",vote_average);

            list.add(map);
        }


        return list;
    }

    public String utEpisodes(String json, JSONArray episodes_array) throws Exception {
        JSONObject object = new JSONObject(json);
        JSONArray array_series = object.getJSONArray("series");
        for (int i = 0; i < array_series.length(); i++) {
        if(episodes_array.length()>0){
            JSONObject object_serie = array_series.getJSONObject(i);
            JSONObject object_episode = episodes_array.getJSONObject(0);

            if (object_serie.get("id").equals(object_episode.get("serieId") + "")){
                object_serie.put("episodes",episodes_array);
                array_series.put(i,object_serie);
                object.put("series",array_series);
                break;
            }


            }
        }

        return object.toString();
    }

    public String  putSeriesList(String json, String name, String id, int number_of_seasons)throws Exception{
        JSONObject js = new JSONObject(json);
        JSONArray jsonArray = js.getJSONArray("series");
            JSONObject nObj = new JSONObject();
            nObj.put("name",name);
            nObj.put("id",id);
            nObj.put("number_of_seasons",number_of_seasons);

                JSONArray episodes = new JSONArray();
                nObj.put("episodes",episodes);

        jsonArray.put(nObj);

        JSONObject outputObj = new JSONObject();
        outputObj.put("series",jsonArray);


        return outputObj.toString();
    }

    public String  putSeriesList(String json, String name, String id, int number_of_seasons,JSONArray episodes)throws Exception{
        JSONObject js = new JSONObject(json);
        JSONArray jsonArray = js.getJSONArray("series");
        JSONObject nObj = new JSONObject();
        nObj.put("name",name);
        nObj.put("id",id);
        nObj.put("number_of_seasons",number_of_seasons);

        nObj.put("episodes",episodes);

        jsonArray.put(nObj);

        JSONObject outputObj = new JSONObject();
        outputObj.put("series",jsonArray);


        return outputObj.toString();
    }

    public List seriesList(String json)throws Exception{
        ArrayList<HashMap<String,Object>> jsonlist = new ArrayList<>();

        JSONObject obj = new JSONObject(json);
        JSONArray jsonArray = obj.getJSONArray("series");

        for(int i=0;i<jsonArray.length();i++){
            JSONObject o = jsonArray.getJSONObject(i);
            String name = o.getString("name");
            int id = o.getInt("id");
            int number_of_seasons = o.getInt("number_of_seasons");

            HashMap<String,Object> map = new HashMap<>();

            map.put("name",name);
            map.put("id",id);
            map.put("number_of_seasons",number_of_seasons);

            jsonlist.add(map);
        }

        return jsonlist;
    }


    public List searchSerie(String json) throws Exception{
        ArrayList<HashMap<String,String>> jsonlist = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(json);
        JSONArray jarray = jsonObject.getJSONArray("results");
        for(int i =0;i<jarray.length();i++){
            JSONObject jobject = jarray.getJSONObject(i);
            String poster_path = jobject.getString("poster_path");
            String id ="" + jobject.getInt("id");
            String name = jobject.getString("name");
            String vote_average = jobject.getString("vote_average");

            HashMap<String,String> map = new HashMap<>();

            map.put("id",id);
            map.put("name",name);
            map.put("poster_path",poster_path);
            map.put("vote_average",vote_average);

            jsonlist.add(map);



        }
        return jsonlist;
    }

    public List searchSerieInfo(String json) {
        ArrayList<HashMap<String,Object>> jsonlist = new ArrayList<>();

        try{
            JSONObject obj = new JSONObject(json);
            String name = obj.getString("original_name");
            int number_of_seasons = obj.getInt("number_of_seasons");
            String poster_path = obj.getString("poster_path");
            String description = obj.getString("overview");
            String vote_average = obj.getString("vote_average");

            HashMap<String, Object> map = new HashMap<>();

            map.put("name",name);
            map.put("number_of_seasons",number_of_seasons);
            map.put("poster_path",poster_path);
            map.put("desc",description);
            map.put("vote_average",vote_average);

            jsonlist.add(map);
        }catch(Exception e){

        }


        return jsonlist;
    }

    public ArrayList<ArrayList<HashMap<String,Object>>> parseEpisodes(String json){
        ArrayList<ArrayList<HashMap<String,Object>>> list = new ArrayList<>();
        try{
            JSONObject main_object = new JSONObject(json);
                JSONArray array_series = main_object.getJSONArray("series");
                for(int i=0;i<array_series.length();i++){
                    ArrayList<HashMap<String,Object>> serie = new ArrayList<>();
                    JSONObject serie_object = array_series.getJSONObject(i);
                    JSONArray episodes_array = serie_object.getJSONArray("episodes");
                        for(int e=0;e<episodes_array.length();e++){
                            JSONObject episode = episodes_array.getJSONObject(e);
                            HashMap<String,Object> map = new HashMap<>();

                            map.put("serie",serie_object.get("name"));
                            map.put("name",episode.get("name"));
                            map.put("air_date",episode.get("air_date"));

                            serie.add(map);
                        }
                    list.add(serie);
                }
        }catch(Exception e){

        }


        return list;
    }

    public List getEpisodes(String json){
        ArrayList<HashMap<String,Object>> jsonlist = new ArrayList<>();
        try{
            JSONObject jsono = new JSONObject(json);
            JSONArray episodesarray = jsono.getJSONArray("episodes");
            for(int i =0;i<episodesarray.length();i++){
                JSONObject obj =episodesarray.getJSONObject(i);
                String name = obj.getString("name");
                String air_date = obj.getString("air_date");
                int id = obj.getInt("id");
                String overview = obj.getString("overview");
                int season_number = obj.getInt("season_number");
                int number_of_episodes = episodesarray.length();

                HashMap<String,Object> map = new HashMap<>();

                map.put("name",name);
                map.put("air_date",air_date);
                map.put("id",id);
                map.put("overview",overview);
                map.put("season_number",season_number);
                map.put("number_of_episodes",number_of_episodes);

                jsonlist.add(map);


            }
        }catch(Exception e){

        }
        return jsonlist;
    }


}
